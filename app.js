// Hello World otimizado em Docker image

var express = require('express');

// Constants
const PORT = process.env.PORT || 8000;

// Aplicação "hello world"
var app = express();

var server = app.listen(PORT, function () {
    console.log('Webserver is ready');
});

app.get('/', function (req, res) {
  res.send('Hello Docker World\n');
});

module.exports = app;