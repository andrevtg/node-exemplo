FROM node:alpine

RUN mkdir -p /opt/app

WORKDIR /opt/app
COPY . .
RUN npm install

CMD [ "npm", "start" ]
